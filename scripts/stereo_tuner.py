#!/usr/bin/env python
import rospy
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

class StereoTuner:

    def left_callback(self, data):
        self.left_image = self.bridge.imgmsg_to_cv2(data, desired_encoding='passthrough')
        self.left_avail = True
        if self.right_avail:
            self.update_depth_image()
    def right_callback(self, data):
        self.right_image = self.bridge.imgmsg_to_cv2(data, desired_encoding='passthrough')
        self.right_avail = True
        if self.left_avail:
            self.update_depth_image()

    def update_depth_image(self):
        #left = cv2.cvtColor(self.left_image, cv2.COLOR_BGR2GRAY)
        #right = cv2.cvtColor(self.right_image, cv2.COLOR_BGR2GRAY)
        left = self.left_image[:,:,1]
        right = self.right_image[:,:,1]
        self.left_avail = False
        self.right_avail = False

        self.depth_image = self.stereo.compute(left, right).astype("uint8")

        stackedImg = cv2.hconcat([left, right])
        stackedImg = cv2.resize(stackedImg, (stackedImg.shape[1]/2, stackedImg.shape[0]/2))
        stackedImg = cv2.vconcat([stackedImg, self.depth_image])

        self.display_image = stackedImg
        
    def __init__(self, left_topic, right_topic, depth_topic):
        rospy.init_node('depth_pub', anonymous = True)
        
        left_sub = rospy.Subscriber(left_topic, Image, self.left_callback)
        right_sub = rospy.Subscriber(right_topic, Image, self.right_callback)

        self.disp_val = 32 
        self.block_size = 19 

        self.rate = rospy.Rate(30)
        self.stereo = cv2.StereoBM_create(numDisparities=self.disp_val, blockSize=self.block_size)
        self.left_avail = False
        self.right_avail = False
        self.display_image = None
        self.bridge = CvBridge()

    def update(self):
        self.rate.sleep()

    def get_image(self):
        return self.display_image

    def set_disp_val(self, x):
        self.disp_val = x
        self.stereo = cv2.StereoBM_create(numDisparities=self.disp_val, blockSize=self.block_size)
    def set_block_size(self, x):
        self.block_size = x
        self.stereo = cv2.StereoBM_create(numDisparities=self.disp_val, blockSize=self.block_size)

depth_pub = None        

def set_disp_val(x):
    depth_pub.set_disp_val(16 * (x+1))

def set_block_size(x):
    depth_pub.set_block_size(2*x+5)

def nothing(x):
    pass

if __name__ == "__main__":
    cv2.namedWindow('Depth Test')
    cv2.createTrackbar('NumDisp','Depth Test',1,10, set_disp_val)
    cv2.createTrackbar('BlockSize','Depth Test',7,40, set_block_size)
    try:
        left_topic = "/qubo_gazebo/qubo_gazebo/cameraleft/camera_image"
        right_topic = "/qubo_gazebo/qubo_gazebo/cameraright/camera_image"
        depth_topic = "/qubo_gazebo/depth_image"
        depth_pub = StereoTuner(left_topic, right_topic, depth_topic)

        while not rospy.is_shutdown():
            depth_pub.update()
            img = depth_pub.get_image()
            cv2.imshow("Depth Test", img)
            if cv2.waitKey(30) == ord('q'):
                break
    except:
        pass
